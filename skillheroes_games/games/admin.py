from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Game, GameType

# Register your models here.


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ["name", "game_type", "price", "date_modified"]
    list_filter = ["game_type"]
    ordering = ["name"]
    search_fields = ["name"]
    fieldsets = (
        (
            _("General"),
            {"fields": ["name", "image", "price", "game_type", "play_time"]},
        ),
        (_("Players"), {"fields": ["min_players", "max_players"]}),
        (
            _("Age restrictions"),
            {"fields": ["min_age_restriction", "max_age_restriction"]},
        ),
    )


@admin.register(GameType)
class GameTypeAdmin(admin.ModelAdmin):
    list_display = ["name", "date_modified"]
    ordering = ["name"]
    search_fields = ["name"]

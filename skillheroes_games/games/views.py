from django.shortcuts import render
from django.views.generic.list import ListView

from .models import Game


class GameListView(ListView):
    model = Game

    def get_queryset(self):
        qs = super().get_queryset()
        # This is an optimization because it fetches the game_type model's name in the same query
        qs = qs.select_related("game_type")
        return qs


class GameFancyListView(ListView):
    model = Game
    template_name = "games/game_list_fancy.html"

    def get_queryset(self):
        qs = super().get_queryset()
        # This is an optimization because it fetches the game_type model's name in the same query
        qs = qs.select_related("game_type")
        return qs

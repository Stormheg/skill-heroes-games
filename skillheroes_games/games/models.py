from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail import ImageField


#: GameType model
class GameType(models.Model):
    date_added = models.DateTimeField(_("added"), auto_now_add=True)
    date_modified = models.DateTimeField(_("modified"), auto_now=True)

    name = models.CharField(_("name"), max_length=50)

    class Meta:
        verbose_name = _("game type")
        verbose_name_plural = _("game types")

    def __str__(self):
        return self.name


#: Game model
class Game(models.Model):
    date_added = models.DateTimeField(_("added"), auto_now_add=True)
    date_modified = models.DateTimeField(_("modified"), auto_now=True)

    image = ImageField(_("image"), upload_to="games")

    name = models.CharField(_("name"), max_length=50)
    price = models.DecimalField(_("price"), max_digits=5, decimal_places=2)

    game_type = models.ForeignKey(
        "games.GameType", verbose_name=_("game type"), on_delete=models.CASCADE
    )

    min_players = models.IntegerField(_("minimum number of players"))
    max_players = models.IntegerField(_("maximum number of players"))

    min_age_restriction = models.IntegerField(_("minimum age in years"))
    max_age_restriction = models.IntegerField(_("maximum age in years"))

    play_time = models.IntegerField(_("average play time in minutes"))

    class Meta:
        verbose_name = _("game")
        verbose_name_plural = _("games")

    def get_players(self):
        return "{} - {}".format(self.min_players, self.max_players)

    def get_age_restrictions(self):
        return "{} - {}".format(
            self.min_age_restriction, self.max_age_restriction
        )

    def __str__(self):
        return self.name

# skillheroes_games

Application for Skill Heroes competition.

## Requirements

- Python 3.7

## Getting started

1. Create and enter a new virtualenv.

```bash
python3 -m venv venv
source venv/bin/activate
```

2. Install dependencies with pip.

```bash
pip install -r requirements.txt
pip install -r requirements_dev.txt

```

3.  Migrate the database

```bash
python manage.py migrate
```

4. Create a new superuser

```bash
python manage.py createsuperuser
```

5. Start the development server

```bash
python manage.py runserver
```
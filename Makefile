
clean:
	find . -name '*.pyc' -exec rm -rf {} +
	find . -name '__pycache__' -exec rm -rf {} +
	find . -name '*.egg-info' -exec rm -rf {} +

compilemessages:
	./manage.py compilemessages -l nl

makemessages:
	./manage.py makemessages --all

lint:
	isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --use-parentheses --line-width=81 --recursive --check-only --diff skillheroes_games
	black --check -l 81 skillheroes_games

format:
	isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --use-parentheses --line-width=81 --recursive skillheroes_games
	black -l 81 skillheroes_games
